var express = require('express');
var router = express.Router();
var utilHome = require('./middlewares/utilHome')

/* GET home page. */
//router.all('/',utilHome.renderHome);
router.all('/',utilHome.setLanguage('vi'),utilHome.renderHome);

router.all('/vi', utilHome.setLanguage('vi'),(req, res, next) => {res.redirect('back')});
router.all('/en', utilHome.setLanguage('en'),(req, res, next) => {res.redirect('back')});

router.all('/happy',utilHome.renderHome);
router.all('/love',utilHome.renderLove);
router.all('/smile',utilHome.renderSmile);

router.all('/aboutUs',utilHome.renderAboutUs);

router.all('/addStory',utilHome.renderAddStory);

router.all("/loveStory", utilHome.loveStory);
router.all("/viewCount", utilHome.viewCount);

router.all("/revise", utilHome.renderRevise)
router.all("/storyAction", utilHome.storyAction)
router.all("/postSuccess", utilHome.renderPostSuccess)

module.exports = router;
