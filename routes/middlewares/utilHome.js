const i18n = require('i18n')
const db = require('../../common/database')
const cfg = require('../../common/config')
const uuidv1 = require('uuid/v1');
const fetch = require('isomorphic-fetch');

const resize = require('../../common/resize')

const postPerPage = 9;
const maxPagnition = 3;
const storyType = {
    any: 0,
    love : 1,
    smile: 2
}

async function pagnitionParams( pageNo, type, isActivate ){
    let pagnitionArr = [];
    let pageCount;
    pageNo = parseInt(pageNo,10)
    let query;
    query = type==0 ?  `select count(*) as storyCount from story where isActivate = ${isActivate} `
                       : `select count(*) as storyCount from story where type = ${type} and isActivate = ${isActivate}`

    await db.query(query).then(result => {
        if(result.length <= 0) return 0;
        pageCount = Math.ceil(result[0]['storyCount'] / postPerPage);
        if(pageNo <0 || pageNo > pageCount) pageNo = 1;
        for(i = 1; i<= Math.min(maxPagnition,pageCount); i++){
            pagnitionArr.push({page: i, isActive: (i==pageNo), url:makePageURL(isActivate,type, i)});
        }
        if(maxPagnition < pageCount){
            // case 1,2,3..pageCount
            if(pageNo > maxPagnition && pageNo!=pageCount){
                if(pageNo - maxPagnition >= maxPagnition){
                    pagnitionArr.push({page: -1, isActive: false},{page:pageNo, isActive: true, url:makePageURL(isActivate,type, pageNo)})    
                }
                else{
                    for(i= maxPagnition+1; i<=pageNo; i++ ){
                        pagnitionArr.push({page: i, isActive: (i==pageNo), url:makePageURL(isActivate,type, i)})    
                    }
                }
                
            }
            if(pageCount - pageNo >= maxPagnition){
                pagnitionArr.push({page: -1, isActive: false}, {page:pageCount, isActive: false, url:makePageURL(isActivate,type, pageNo)})    
            }
            else{
                for(i=pageNo+1; i <= pageCount; i++ ){
                    pagnitionArr.push({page: i, isActive: (i==pageNo), url:makePageURL(isActivate,type, i)})    
                }
            }
        }
        
    })
    
    return {pageNo, pageCount, pagnitionArr}
}

async function extractOGMetadata(story){
    let og = {};
    // general
    if(story==null){
        og.title = "Cô Vịt";
        og.image = "http://covit.vn/heroes.jpg";
        og.description = " Cô Vịt 19 Tuổi";
        og.url = "http://covit.vn"
        og.type="website"
    }
    else{
        og.title = story.storyName;
        og.image = "http://covit.vn"+story.photoURL;
        og.description = story.description;
        og.url = "http://covit.vn/happy?story=" +story.id; 
        og.type= story.visualForm == 0? "photo" :"video.other"
    }
    return og;
}

async function getStory(type, pageNo, isActivate){
    //let story=[];
    
    let query;
    query = type==0 ?  `select * from story where isActivate = ${isActivate} order by uploadDate desc limit ${(pageNo-1)* postPerPage}, ${postPerPage} `
                       : `select * from story where isActivate = ${isActivate} and type = ${type} order by uploadDate desc limit ${(pageNo-1)* postPerPage}, ${postPerPage} `
    story = await db.query(query)
    story.forEach((s) => {
        if(s.location) {
            let index = parseInt(s.location)
            //console.log("location: ",index, " - ", cfg.location[index-1].name, " @ ", cfg.location[index-1].id )
            s.location = cfg.location[index-1].name
        }
    })
    
    return story
}

async function getStoryById(storyId){
    let query;
    query = `select * from story where isActivate = 1 and id = ${storyId}`
    story = await db.query(query)
    let s = story[0]
    if(s.location) {
        let index = parseInt(s.location)
        //console.log("location: ",index, " - ", cfg.location[index-1].name, " @ ", cfg.location[index-1].id )
        s.location = cfg.location[index-1].name
    }
    return s;
}

function makePageURL(isActivate,type, page) {
    let url = '';
    
    switch (type) {
        case 0: url = url + '/happy?p=' + page
            break;
        case 1: url = url + '/love?p=' + page
            break;
        case 2: url = url + '/smile?p=' + page
            break;
    }

    // revise case
    if(isActivate == 0) url = '/revise?p='+page;
    return url;
}

module.exports = {
    setLanguage : function (language){
        return async (req, res, next) =>{
            if(language == ' '){
                await i18n.setLocale(req,req.cookies.lang)
            }
            else if(language == req.cookies.lang){

            }
            else{
                res.cookie('lang', language,{maxAge: 900000});
                await i18n.setLocale(req,language)
            }
            next();
        }
    },

    renderHome : async function (req, res, next){
        let pageNo = req.query.p;
        
        // handling request with page number
        if(!pageNo || typeof pageNo =='undefined') {pageNo = 1;}
        let pageParams = await pagnitionParams(pageNo,storyType.any,1);
        let story = await getStory(storyType.any, pageParams.pageNo, 1);

        // handling request with storyid
        let storyId = req.query.story;
        let specificStory = null;
        if(storyId && typeof storyId !='undefined'){
            specificStory = await getStoryById(storyId);
        }

        let og = await extractOGMetadata(specificStory);
        console.log(og)

        res.render('index', {type:storyType.any, story, pageParams, specificStory, og});
        
    }, 

    renderLove : async function (req, res, next){
        
        let pageNo = req.query.p;
        if(!pageNo || typeof pageNo =='undefined') {pageNo = 1;}
        
        let pageParams = await pagnitionParams(pageNo,storyType.love,1);
        let story = await getStory(storyType.love, pageParams.pageNo, 1);

        // handling request with storyid
        let storyId = req.query.story;
        let specificStory = null;
        if(storyId && typeof storyId !='undefined'){
            specificStory = await getStoryById(storyId);
        }
        let og = extractOGMetadata(specificStory)


        res.render('index', {type:storyType.love,story,pageParams, specificStory, og})
    }, 

    renderSmile : async function (req, res, next){
        let isLove = false;
    
        let pageNo = req.query.p;
        if(!pageNo || typeof pageNo =='undefined') {pageNo = 1;}
        
        let pageParams = await pagnitionParams(pageNo,storyType.smile,1);
        
        let story = await getStory(storyType.smile, pageParams.pageNo, 1);

        // handling request with storyid
        let storyId = req.query.story;
        let specificStory = null;
        if(storyId && typeof storyId !='undefined'){
            specificStory = await getStoryById(storyId);
        }

        let og = extractOGMetadata(specificStory)
        res.render('index', {type:storyType.smile,story,pageParams, specificStory,og})
    }, 

    renderAboutUs : function (req, res, next){
        res.render('about', { og: extractOGMetadata(null)});
    },


    renderAddStory : async function (req, res, next)
    {
        if (req.method == 'GET') {
            res.render('addStory', { og: extractOGMetadata(null), location: cfg.location });
        }
        else{
            let {isname, ianame, isource, itakendate, ilocation, idescription, itag, tac, icategories, gRecaptchaResponse, visualForm} = req.body;
            console.log(visualForm)
            let errors = [];
            let file;
            let imgPath = '';
            if(req.files != null){
                if(Object.keys(req.files).length >0){
                    file = req.files.uploadPhoto
                }

            }
            else{
                if(visualForm)
                    errors.push(i18n.__('Tell a story with picture!'))
            }

            
            if(gRecaptchaResponse === undefined || gRecaptchaResponse === '' || gRecaptchaResponse === null)
            {
                return res.json({"responseError" : "something goes to wrong"});
            }

            const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + cfg.site.secretKey + "&response=" + gRecaptchaResponse //+ "&amp;remoteip=" + req.connection.remoteAddress;
            let gResponse;
            fetch(verificationURL, {
                method: 'post'
            })
                .then(response => response.json())
                .then(google_response => {
                    gResponse = google_response
                    //console.log("response: ",google_response);
                    if(!gResponse.success) {
                        res.redirect('/happy')
                    }
                })
                .catch(error => console.log(error));
            
            
            
            if(!isname || !ianame || !tac){
                errors.push(i18n.__('Please input required* information'))
            }

            if(visualForm)  visualForm = 0;     //photo story
            else visualForm = 1;                //video story

            
            if(visualForm == 1){
                //console.log(visualForm)
                if (isource != undefined || isource != '') {
                    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
                    var match = isource.match(regExp);
                    if (match && match[2].length == 11) {
                        // Do anything for being valid
                        imgPath= `https://img.youtube.com/vi/` + match[2]+ `/0.jpg`
                        isource =  'https://www.youtube.com/embed/' + match[2] + '?autoplay=0'
                    }
                    else {
                        // Do anything for not being valid
                        errors.push(i18n.__('Youtube video link is incorrect!'))
                    }
                }
            }
            
            if(errors.length > 0 ){
                res.render('addStory',{location: cfg.location , errors})
            }
            else{
                let tagList= [];
                let uid = uuidv1();
                
                if(itag){
                    tagList = itag.trim().split(",")
                    let idx = 0;
                    tagList.forEach(t => {
                        t = t.trim();
                        if(t.length >=2 ){
                            idx ++;
                        }
                        else{
                            tagList.splice(idx)
                        }
                    })
                }
                if(visualForm == 0){
                    let extension = file.name.split('.').pop();

                    imgPath = './public/upload/' + uid.slice(0,15) + '.' + extension;
                    thumbPath = './public/upload/thumb/'+ uid.slice(0,15) + '.' + extension;
                    file.mv(imgPath, function(err){
                        if(err){
                            return res.status(500).send(err);
                        }
                    })
                }
                    
                
                
                db.query("start transaction;")
                let takendate = !itakendate ? "now()" : itakendate.slice(0, 10);
                let query = `insert into story(storyname, referenceLink, authorName, description, photoURL, uploadDate, takenDate, location, type,isActivate,visualForm)` +
                            `values(${db.escape(isname)}, ${db.escape(isource)}, ${db.escape(ianame)}, ${db.escape(idescription)}, ${db.escape(imgPath)}, now(), ${takendate}, ${ilocation}, ${icategories}, 0, ${visualForm})`
                // console.log(query)

                ret = await db.query(query)
                
                tagList.forEach((t)=>{
                    db.query(`insert into story_tag(storyId, tag) values(${ret.insertId}, ${db.escape(t)})`)
                })
                db.query("commit")
                //console.log("redirect")
                res.redirect('/postSuccess')
                if(visualForm == 0)
                    resize(file,imgPath,thumbPath)
                
            }
        }
    },

    loveStory : async function(req, res, next){
        let query = `update story set love = love + 1 where id = ${req.body.id}`
        db.query(query);
        res.json({ret:'OK'})
    },

    viewCount : async function(req, res, next){
        let query = `update story set viewCount = viewCount + 1 where id = ${req.body.id}`
        db.query(query);
        res.json({ret:'OK'})
    },


    renderRevise : async function(req,res,next){
        let pageNo = req.query.p;
        if(!pageNo || typeof pageNo =='undefined') {pageNo = 1;}
        
        let pageParams = await pagnitionParams(pageNo,storyType.any,0);
        let story = await getStory(storyType.any, pageParams.pageNo,0);
        // console.log(pageParams)
        res.render('revise', {type:storyType.any, story, pageParams,og: extractOGMetadata(null)});
    },

    storyAction: async function(req, res, next){
        
        let gRecaptchaResponse = req.body.gRecaptchaResponse
        if (gRecaptchaResponse === undefined || gRecaptchaResponse === '' || gRecaptchaResponse === null) {
            return res.json({ "responseError": "something goes to wrong" });
        }

        const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + cfg.site.secretKey + "&response=" + gRecaptchaResponse //+ "&amp;remoteip=" + req.connection.remoteAddress;
        let gResponse;
        fetch(verificationURL, {
            method: 'post'
        })
            .then(response => response.json())
            .then(google_response => {
                gResponse = google_response
                // console.log("response: ",google_response);
                if (!gResponse.success) {
                    // console.log("that bai")
                    res.redirect('/happy')
                }
                else{
                    // console.log("thanh cong")
                    let isActivate = req.body.accept? 1:0;
                    let query = `update story set isActivate= ${isActivate} where id = ${req.body.id}`
                    db.query(query);
                    res.json({ret:'OK'})
                }
            })
            .catch(error => console.log(error));
        
    },
    
    
    renderPostSuccess : function(req, res, next){
        res.render("postSuccess", {og: extractOGMetadata(null)});
    }
}