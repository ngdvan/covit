var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var expressLayouts = require('express-ejs-layouts')
var compression = require('compression')
var session = require('express-session');
var i18n = require("i18n");
const cfg = require('./common/config')
const flash = require('connect-flash')
const fileUpload = require('express-fileupload');
const db = require('./common/database')
var MySQLStore = require('express-mysql-session')(session);


var indexRouter = require('./routes/index');

var app = express();

// DB Setup
let conn = db.connect();
var sessionStore = new MySQLStore({},db);
// view engine setup
app.use(expressLayouts);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout','layouts/main');
app.set("layout extractScripts", true);
app.set("layout extractStyles", true);

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Express session
app.use(
  session({
    key: 'covit_vn',
    secret: "f5021625-037b-47a7-b8f3-ffe721bb3a0b",
    resave: false,
    store:sessionStore,
    saveUninitialized: false,
  })
);

app.use(cookieParser());
app.disable("x-powered-by");
app.use(flash())


// Locale
app.use(i18n.init);
i18n.configure({
  locales:['en', 'vi'],
  directory: __dirname + '/locales',
  cookie: 'lang',
  defaultLocale: 'vi'
 });

// static 
app.use('/public',express.static(path.join(__dirname, 'public')));
app.use('/img',express.static(path.join(__dirname, 'public/img')));
app.use('/css',express.static(path.join(__dirname, 'public/css')));
app.use('/js',express.static(path.join(__dirname, 'public/js')));
app.use('/upload',express.static(path.join(__dirname, 'public/upload')));

app.use(fileUpload());

// Routing


app.use('/', indexRouter);
// app.use('/', (req, res, next)=>{res.json({res:"ok"})});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  

  // render the error page
  res.status(err.status || 500);
  res.render('default/error',{layout:false});
});



module.exports = app;
