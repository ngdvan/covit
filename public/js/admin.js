$('#accept-story-btn').on('click', function(e){
    let id = parseInt($('#accept-story-btn').attr('value'));
    $.post("/storyAction", { id, accept:true }, function (data) {
        $('#accept-story-btn').find('#status').text('Accepted')
    })
})

$('#reject-story-btn').on('click', function(e){
    let id = parseInt($('#reject-story-btn').attr('value'));
    $.post("/storyAction", { id, accept:false }, function (data) {
        $('#accept-story-btn').find('#status').text('Rejected')
    })
})

$('#photo-modal').on('hide.bs.modal', function (event) {
    let story = stories[getStory(currentModalStoryId)];
    if(story.visualForm == 1){
        $('#youtube-player').attr("src","")
    }
})

$('#photo-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    currentModalStoryId = button.data('id') // Extract info from data-* attributes
    let story = stories[getStory(currentModalStoryId)];
    
    if(story.visualForm == 0){
        $('#photo-output-field').removeClass("d-none");
        $('#video-output-field').addClass("d-none");
        $('#modal-image').attr('src', story.photoURL);
        
    }
        
    else if(story.visualForm == 1){
        $('#photo-output-field').addClass("d-none");
        $('#video-output-field').removeClass("d-none");
        $('#youtube-player').attr('src', story.referenceLink);
    }


    $.post("/viewCount", { id: story.id.toString() }, function (data) {
        stories.viewCount++;
    })

    if (story.type == 1) {
        $('#modal-story-type').addClass('fa fa-heart')
    }
    else {
        $('#modal-story-type').addClass('fa fa-smile-o')
    }
    $('#modal-story-name').text(story.storyName);
    $('#modal-story-author').text(story.authorName);

    if (story.description != null && story.description != undefined){
        // console.log(story.description)
        $('#modal-story-description').text(story.description);
    }
    else
        $('#modal-story-description').text("");

        
    if (story.location) {
        $('#location-icon').addClass('fa fa-map-marker')
        $('#modal-story-location').text(story.location);
    }
    else{
        $('#location-icon').removeClass('fa fa-map-marker')
        $('#modal-story-location').text("");
    }
    

    if (story.uploadDate)
        $('#modal-story-date').text(story.uploadDate.substr(0, story.uploadDate.indexOf('T')))
    else
        $('#modal-story-date').text("")

    if (story.love) {
        $('#modal-story-love').text(story.love + " ")
    }
})