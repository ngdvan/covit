let currentModalStoryId;
function getStory(id) {
    for (idx = 0; idx < stories.length; idx++) {
        if (stories[idx].id == parseInt(id))
            return idx;
    }
    return -1;
};

function showModal(story){
    if(story.visualForm == 0){
        $('#photo-output-field').removeClass("d-none");
        $('#video-output-field').addClass("d-none");
        $('#modal-image').attr('src', story.photoURL);
    }
        
    else if(story.visualForm == 1){
        $('#photo-output-field').addClass("d-none");
        $('#video-output-field').removeClass("d-none");
        $('#youtube-player').attr('src', story.referenceLink);
    }


    $.post("/viewCount", { id: story.id.toString() }, function (data) {
        stories.viewCount++;
    })

    if (story.type == 1) {
        $('#modal-story-type').addClass('fa fa-heart')
    }
    else {
        $('#modal-story-type').addClass('fa fa-smile-o')
    }
    $('#modal-story-name').text(story.storyName);
    $('#modal-story-author').text(story.authorName);

    if (story.description != null && story.description != undefined){
        // console.log(story.description)
        $('#modal-story-description').text(story.description);
    }
    else
        $('#modal-story-description').text("");

        
    if (story.location) {
        $('#location-icon').addClass('fa fa-map-marker')
        $('#modal-story-location').text(story.location);
    }
    else{
        $('#location-icon').removeClass('fa fa-map-marker')
        $('#modal-story-location').text("");
    }
    

    if (story.uploadDate)
        $('#modal-story-date').text(story.uploadDate.substr(0, story.uploadDate.indexOf('T')))
    else
        $('#modal-story-date').text("")

    if (story.love) {
        $('#modal-story-love').text(story.love + " ")
    }
}

$(function() {
    let currentModalStoryId = -1;
    let firstVisitSpecific = false;
    
    if(specificStory && specificStory.id){
        firstVisitSpecific = true;
    }

    if(firstVisitSpecific){
        showModal(specificStory)
        $('#photo-modal').modal('show')
        
    }
    

    $('#love-btn').click(() => {
        window.location = '/love'
    })

    $('#happy-btn').click(() => {
        window.location = '/happy'
    })

    $('#smile-btn').click(() => {
        window.location = '/smile'
    })

    $('#love-story-btn').click(() => {
        let story;
        if(firstVisitSpecific)
            story = specificStory;
        else
            story = stories[getStory(currentModalStoryId)];
        story.love++;

        $.post("/loveStory", { id: story.id.toString() }, function () {
            $('#modal-story-love').text(story.love + " ")
        })
    })

    $('#photo-modal').on('hide.bs.modal', function (event) {
        if(firstVisitSpecific){
            firstVisitSpecific = false;
            window.location = '/happy'
        }
        else{
            let story = stories[getStory(currentModalStoryId)];
            if(story.visualForm == 1){
                $('#youtube-player').attr("src","")
            }
            history.back();
        }
        
    })

    $('#photo-modal').on('show.bs.modal', function (event) {
        
        let story;
        if(firstVisitSpecific){
            // console.log("specific: ",specificStory);
            story = specificStory;
        }
        else{
            var button = $(event.relatedTarget) // Button that triggered the modal
            currentModalStoryId = button.data('id') // Extract info from data-* attributes
            story = stories[getStory(currentModalStoryId)];
            // console.log("regular: " + story);
            history.pushState(null,null,'?story=' + currentModalStoryId)
        }
        // console.log("Story name: ", story.storyname)        
        showModal(story)
        
    })

    
    
})

