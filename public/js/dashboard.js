$(function () {
  // Profile picture section  
    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }

    let imgCrop
    $('#profilePic').on('change.bs.fileinput', function (e) {
      $('#profilePicPreview').children('img').attr({'class':'d-none', 'style':'max-width:none'})
      let previewSrc = $('#profilePicPreview').children('img').attr('src')
      $('#editProfilePicImg').attr('src', previewSrc)
      $("#profilePicModal").modal('show')
    })

    $('#discardProfilePicBtn').on('click',(e)=>{
      $('#profilePicPreview').croppie('destroy');
    })

    $('#saveProfilePicBtn').on('click', (e) => {
      e.preventDefault();
      imgCrop.croppie("result", {
        type: "canvas",
        size: 'original'
      }).then(function(resp) {
        console.log(resp)
        var block = resp.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];
        
        var blob = b64toBlob(realData, contentType)
        var filename = "profile."+contentType.split("/")[1]
        var fd = new FormData();
        fd.append('file', blob,filename);
        $.ajax({
          type: 'POST',
          url: '/users/dashboard/changeProfilePic',
          data: fd,
          contentType: false,
          processData: false,
          success: function () {
            console.log('profile picture submitted');
            $('#profilePictureIN').attr('src',resp)
          }
        });
      });
      
      $('#profilePicPreview').croppie('destroy');
    })

    $("#profilePicModal").on('shown.bs.modal', function (e) {
      $('#editProfilePicImg').croppie('destroy')
      if(!$('#editProfilePicImg').data('croppie')){
        imgCrop = $('#editProfilePicImg').croppie({
          boundary: {
            width: 200,
            height: 200,
          },
          viewport: {
            width: 150,
            height: 150,
            type:'circle',
          }
        })
        
        imgCrop.croppie('bind',{
          url: $('#editProfilePicImg').attr('src'),
          zoom: '1'
        })
        
        
      }
    })
    $('#modalCropBtn').on('click', function(e){
      
      e.preventDefault();
      imgCrop.croppie("result", {
        type: "canvas",
        size: 'original'
      }).then(function(resp) {
        console.log(resp)
        var block = resp.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];
        
        var blob = b64toBlob(realData, contentType)
        var filename = "profile."+contentType.split("/")[1]
        var fd = new FormData();
        fd.append('file', blob,filename);
        $.ajax({
          type: 'POST',
          url: '/users/dashboard/changeProfilePic',
          data: fd,
          contentType: false,
          processData: false,
          success: function () {
            console.log('profile picture submitted');
            $('#profilePictureIN').attr('src',resp)
          }
        });
      });
      $('#editProfilePicImg').croppie('destroy')
      $("#profilePicModal").modal('hide')
      $('.fileinput').fileinput('clear')
    })

    $('#modalDiscardBtn').on('click', function(){
      $('.fileinput').fileinput('reset')
    })


// Data section
    $('#bday').datetimepicker({
      format: 'D/MM/YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });

  // Tab section
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var target = $(e.target).attr("href") // activated tab
      
    });

    let url = location.href.replace(/\/$/, "");
 
    if (location.hash) {
      const hash = url.split("#");
      $('#profileSetupTab a[href="#'+hash[1]+'"]').tab("show");
      url = location.href.replace(/\/#/, "#");
      history.replaceState(null, null, url);
      setTimeout(() => {
        $(window).scrollTop(0);
      }, 400);
    } 
    
    $('a[data-toggle="tab"]').on("click", function() {
      let newUrl;
      const hash = $(this).attr("href");
      
      newUrl = url.split("#")[0] + hash;
      
      newUrl += "/";
      history.replaceState(null, null, newUrl);
    });


  })