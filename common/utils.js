module.exports= {
  ensureAuthenticated: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error_msg', 'Bạn cần đăng nhập để xem được trang này');
    res.redirect('/users/login');
  },
  forwardAuthenticated: function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    res.redirect('/');      
  },
  
  checkAuthenticated: function(req, res, next){
    if (!req.isAuthenticated()) {
      return next();
    }
    res.redirect('/');      
  }
}