const fs = require('fs')
const sharp = require('sharp')
var sizeOf = require('image-size');

module.exports = function resize(file, imgPath, thumbPath) {
  sizeOf(imgPath, async function (err, dimensions) {
    console.log("start resizing");
    if(dimensions.width > 300 || dimensions.height > 200){
        let ratio = dimensions.width / dimensions.height;
        let thumbW, thumbH;
        // landscape
        if(dimensions.width > dimensions.height){
             thumbW = Math.min(dimensions.width, 300);
             thumbH = Math.ceil(thumbW / ratio);
        }
        else{
            thumbH = Math.min(dimensions.height, 200);
            thumbW = Math.ceil(thumbH * ratio);
        }
        const pic = await sharp(imgPath).resize(thumbW,thumbH).toFile(thumbPath, (err, info) => {
          if (err) {
            file.mv(thumbPath, function (err) {
              if (err) {
                return res.status(500).send(err);
              }
            })
          }
        })
        console.log("end resizing");
        return pic;
    }
  });
}