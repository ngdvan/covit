const LocalStrategy = require("passport-local").Strategy;
const RememberMeStrategy = require("passport-remember-me").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const uFeatures = require('../models/userJobFeatures')
const cfg = require("./config");

// Load User model
const {User,
  PhotographerUser,
  ModelUser,
  MakeupArtistUser,
  StylishUser}  = require("../models/userModel");

module.exports = function (passport) {
  // Local Login
  passport.use('local.login',
    new LocalStrategy(
      { usernameField: "email",
        passwordField: "password",
      }, (email, password, done) => {
        // Match user
        User.findOne({
          email: email
        }).then(user => {
          if (!user) {
            return done(null, false, { message: "Email chưa đăng ký" });
          }

          // Match password
          if(user.validPassword(password)){
            return done(null, user);
          }
          else{
            return done(null, false, { message: "Mật khẩu không chính xác" });
          }
        });
      })
  );

  passport.use('local.register',
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true
      },
      (req, email, password, done) => {
        process.nextTick(function () {
          // Match user
          User.findOne({
            email: email
          }).then(user => {
            if (user) {
              return done(null, false, req.flash(__('Email exists!')));
            }
            else {
              console.log(req.body.job)
              let job = req.body.job;
              let newUser;
              switch(job){
                case 1:
                  newUser = new PhotographerUser();
                  assert.ok(newUser instanceof PhotographerUser);
                  break;
                case 2:
                  newUser = new ModelUser();
                  break;
                case 3:
                  newUser = new MakeupArtistUser();
                  break;
                case 4:
                  newUser = new StylishUser();
                  break;
                default:
                  newUser = new User();
              }
              newUser.email = email;
              newUser.password = newUser.generateHash(password)
              newUser.job = parseInt(job);
              let tmp = email.split('@')
              newUser.name = tmp[0]
              newUser.save(function (err) {
                if (err) throw err;
                return done(null, newUser)
              })
            }
          });
        })
    })
  );

  // Remember Me 
  passport.use(
    new RememberMeStrategy(
      function (token, done) {
        Token.consume(token, function (err, user) {
          if (err) {
            return done(err);
          }
          if (!user) {
            return done(null, false);
          }
          return done(null, user);
        });
      },
      function (user, done) {
        var token = utils.generateToken(64);
        Token.save(token, { userId: user.id }, function (err) {
          if (err) {
            return done(err);
          }
          return done(null, token);
        });
      }
    )
  );

  // Facebook Login
  passport.use(
    new FacebookStrategy(
      {
        clientID: cfg.facebook.clientID,
        clientSecret: cfg.facebook.clientSecret,
        callbackURL: cfg.facebook.callbackURL,
        profileFields: cfg.facebook.profileFields
      },
      function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
          User.findOne({ "facebook.id": profile.id }, function (err, user) {
            if (err) {
              return done(null, false, { message: "Email chưa đăng ký" });
            } else if (user) {
              return done(null, user);
            } else {
              // nếu chưa có, tạo mới user
              var newUser = new User();
              // lưu các thông tin cho user
              newUser.facebook.id = profile.id;
              newUser.facebook.token = accessToken;
              newUser.name = profile.displayName; // bạn có thể log đối tượng profile để xem cấu trúc
              newUser.email = profile.emails[0].value; // fb có thể trả lại nhiều email, chúng ta lấy cái đầu tiền
              newUser.facebook.name = profile.displayName; // bạn có thể log đối tượng profile để xem cấu trúc
              newUser.facebook.email = profile.emails[0].value; // fb có thể trả lại nhiều email, chúng ta lấy cái đầu tiền
              // lưu vào db
              newUser.save(function (err) {
                if (err) throw err;
                // nếu thành công, trả lại user
                return done(null, newUser);
              });
            }
          });
        });
      }
    )
  );

  passport.use(new GoogleStrategy({
    clientID: cfg.google.clientID,
    clientSecret: cfg.google.clientSecret,
    callbackURL: cfg.google.callbackURL
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ "google.id": profile.id }, function (err, user) {
      if (err) {
        return done(null, false, { message: "Email chưa đăng ký" });
      } else if (user) {
        return done(null, user);
      } else {
        // nếu chưa có, tạo mới user
        var newUser = new User();
        newUser.google.id = profile.id;
        newUser.google.token = accessToken;
        newUser.name = profile.name;
        newUser.email = profile.emails[0];
        newUser.google.name = profile.name;
        newUser.google.email = profile.emails[0];
        newUser.save(function (err) {
          if (err) throw err;
          // nếu thành công, trả lại user
          return done(null, newUser);
        });
      }
    });
  }
));


  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });
};
